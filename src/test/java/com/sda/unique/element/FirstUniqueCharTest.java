package com.sda.unique.element;

import org.junit.Assert;
import org.junit.Test;

public class FirstUniqueCharTest {

    @Test
    public void getFirstUniqueCharTest1() {

        FirstUniqueChar firstUnique = new FirstUniqueChar();

        Character expectedChar = 'u';
        Character actualChar = firstUnique.getFirstUniqueChar("uaabcdeebdchlh");

        Assert.assertEquals(expectedChar,actualChar);
    }

    @Test
    public void getFirstUniqueTest2() {

        FirstUniqueChar firstUnique = new FirstUniqueChar();

        Character expectedChar = 'a';
        Character actualChar = firstUnique.getFirstUniqueChar("uua");

        Assert.assertEquals(expectedChar,actualChar);
    }
}
