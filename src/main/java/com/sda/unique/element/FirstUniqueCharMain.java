package com.sda.unique.element;

public class FirstUniqueCharMain {

    public static void main(String[] args) {

        FirstUniqueChar firstUnique = new FirstUniqueChar();

        Character result = firstUnique.getFirstUniqueChar("abcdeeab");

        System.out.println(result);
    }
}
