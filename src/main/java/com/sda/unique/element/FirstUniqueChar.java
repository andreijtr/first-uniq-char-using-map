package com.sda.unique.element;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class FirstUniqueChar {

    public Character getFirstUniqueChar(String givenWord) {

        Character unique = ' ';
        Character[] splitWord = new Character[givenWord.length()];
        Map<Character, Integer> characterMap = new LinkedHashMap<>();

        for (int i = 0; i < givenWord.length(); i++) {
            splitWord[i] = (givenWord.charAt(i));
        }

        //insert characters in Map and count how many times appears
        for (int i = 0; i < splitWord.length; i++) {
            int count = 0;
            if (characterMap.containsKey(splitWord[i])) {
                count = characterMap.get(splitWord[i]);
                characterMap.put(splitWord[i], ++count);
            } else {
                characterMap.put(splitWord[i], ++count);
            }
        }

        Set<Character> setKey = characterMap.keySet();

        for (Character character : setKey) {
            if(characterMap.get(character) == 1) {
                unique = character;
                break;
            }
        }

        return unique;
    }
}
